# Implementing a program like CamScanner (Spring 2019)
- Written in Python using OpenCV, Artificial Intelligence course project, Dr. Ahmad Nickabadi
- The program had the ability to autodetect the edges of the paper from an image, even those images which
were not taken exactly from the top. Also, it has the feature of editing the edges of the paper in case of not correctly autodetection. Then, the images passed through some filters with the aim of OpenCV, and the final image could be exported as jpg format.

# Implementing a Real-time Background Extractor (Spring 2019)
- Written in Python using OpenCV, Artificial Intelligence course project, Dr. Ahmad Nickabadi
- The program could detect and extract the background using the grabCut, MOG2, and KNN algorithms.
Therefore, it was able to change the background or add effects to the it from both using a saved image or real-time frames via webcam.